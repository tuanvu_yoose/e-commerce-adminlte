FactoryGirl.define do
  factory :article do
    name "MyString"
    description "MyText"
    content "MyText"
  end
end
